<div class="main-nav-wrapper">
    <div class="container">
      <nav class="main-nav navbar navbar-expand-md" role="navigation">
          <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
          </button><!--//nav-toggle-->

          <div class="navbar-collapse collapse" id="navbar-collapse">
              {{-- <ul class="nav navbar-nav"> --}}
              {{-- {{ $page_id }} --}}
                {{-- {!!menu('frontend')!!} --}}
                <ul class="nav navbar-nav">
                  @php
                    // dd(menu('frontend'))
                    $menu = DB::SELECT("SELECT * FROM menu_items WHERE menu_id = 2 and parent_id is NULL ORDER BY `order` ASC");
                    // $menu->orderBy('order', 'ASC');
                  @endphp
                    @foreach($menu as $menu_item)
                      @php
                        // dd($page_id)
                      @endphp
                        {{-- <li class="nav-item"><a class="nav-link" href="{{ $menu_item->link() }}">{{ $menu_item->title}}</a></li> --}}
                        @php
                          $parent_id = $menu_item->id;
                          $subMenus = DB::SELECT("SELECT * FROM menu_items WHERE parent_id = '$parent_id'");
                          // dd(count($subMenus));
                        @endphp
                        @if (count($subMenus) >= 1)
                          @php
                            // dd($menu_item->url)
                          @endphp
                          <li class="nav-item dropdown">
                              <a class="@if ($page_id === $menu_item->id)
                                active
                              @endif nav-link dropdown-toggle" href="#" id="navbarDropdown-1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Departments<i class="fas fa-angle-down"></i></a>
                              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                @foreach ($subMenus as $sub)
                                  <a class="dropdown-item" href="{{ config('app.url') }}/{{ $sub->url }}">{{ $sub->title }}</a>
                                @endforeach
                              </div><!--//dropdown-menu-->
                          </li>
                        @else
                          <li class="nav-item"><a class="nav-link @if ($page_id === $menu_item->url)
                            active
                          @endif" href="{{ config('app.url') }}/{{ $menu_item->url }}">{{ $menu_item->title}}</a></li>
                        @endif
                    @endforeach
                </ul>

                {{-- @foreach($items as $menu_item)
                    <li><a href="{{ $menu_item->link() }}">{{ $menu_item->title }}</a></li>
                @endforeach --}}
                  {{-- <li class="nav-item"><a class="active nav-link" href="index.html">Home</a></li>
                  <li class="nav-item"><a class="nav-link" href="index.html">About us</a></li>
                  <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown-1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Courses <i class="fas fa-angle-down"></i></a>
                      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                          <a class="dropdown-item" href="courses.html">Course List</a>
                          <a class="dropdown-item" href="course-single.html">Single Course (with image)</a>
                          <a class="dropdown-item" href="course-single-2.html">Single Course (with video)</a>
                      </div><!--//dropdown-menu-->
                  </li>
                  <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown-2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">News <i class="fas fa-angle-down"></i></a>
                      <div class="dropdown-menu" aria-labelledby="navbarDropdown-2">
                          <a class="dropdown-item" href="news.html">News List</a>
                          <a class="dropdown-item" href="news-single.html">Single News (with image)</a>
                          <a class="dropdown-item" href="news-single-2.html">Single News (with video)</a>
                      </div>
                  </li>
                  <li class="nav-item"><a class="nav-link" href="events.html">Events</a></li>
                  <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">Pages <i class="fas fa-angle-down"></i></a>
                      <div class="dropdown-menu">
                          <a class="dropdown-item" href="about.html">About</a>
                          <a class="dropdown-item" href="team.html">Leadership Team</a>
                          <a class="dropdown-item" href="jobs.html">Jobs</a>
                          <a class="dropdown-item" href="job-single.html">Single Job</a>
                          <a class="dropdown-item" href="gallery.html">Gallery (3 columns)</a>
                          <a class="dropdown-item" href="gallery-2.html">Gallery (4 columns)</a>
                          <a class="dropdown-item" href="gallery-3.html">Gallery (2 columns)</a>
                          <a class="dropdown-item" href="gallery-4.html">Gallery (with sidebar)</a>
                          <a class="dropdown-item" href="gallery-album.html">Single Gallery</a>
                          <a class="dropdown-item" href="gallery-album-2.html">Single Gallery (with sidebar)</a>
                          <a class="dropdown-item" href="faq.html">FAQ</a>
                          <a class="dropdown-item" href="privacy.html">Privacy Policy</a>
                          <a class="dropdown-item" href="terms-and-conditions.html">Terms & Conditions</a>
                      </div><!--//dropdown-menu-->
                  </li><!--//dropdown-->

                  <li class="nav-item"><a class="nav-link" href="contact.html">Contact</a></li> --}}
              {{-- </ul><!--//nav--> --}}
          </div><!--//navabr-collapse-->

    </nav><!--//main-nav-->
  </div><!--//container-->
</div>
