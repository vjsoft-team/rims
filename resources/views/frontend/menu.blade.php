<ul class="nav navbar-nav">
  @php
    // dd($items)
  @endphp
    @foreach($items as $menu_item)
        {{-- <li class="nav-item"><a class="nav-link" href="{{ $menu_item->link() }}">{{ $menu_item->title}}</a></li> --}}
        @php
          $parent_id = $menu_item->id;
          $subMenus = DB::SELECT("SELECT * FROM menu_items WHERE parent_id = '$parent_id'");
          // dd(count($subMenus));
        @endphp
        @if (count($subMenus) >= 1)
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown-1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Courses <i class="fas fa-angle-down"></i></a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                @foreach ($subMenus as $sub)
                  <a class="dropdown-item @if ($page_id == $sub->slug)
                    active
                  @endif" href="{{ config('app.url') }}/{{ $sub->url }}">{{ $sub->title }}</a>
                @endforeach
              </div><!--//dropdown-menu-->
          </li>
        @else
          <li class="nav-item"><a class="nav-link @if ($page_id == $menu_item->slug)
            active
          @endif" href="{{ $menu_item->link() }}">{{ $menu_item->title}}</a></li>
        @endif
    @endforeach
</ul>
