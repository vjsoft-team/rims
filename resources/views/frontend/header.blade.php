

<header class="header">
    <div class="top-bar">
        <div class="container">
          {{-- <div class="row">
              <ul class="social-icons col-md-6 col-12 d-none d-md-block">
                  <li><a href="#" ><i class="fab fa-twitter"></i></a></li>
                  <li><a href="#" ><i class="fab fa-facebook-f"></i></a></li>
                  <li><a href="#" ><i class="fab fa-youtube"></i></a></li>
                  <li><a href="#" ><i class="fab fa-linkedin-in"></i></a></li>
                  <li><a href="#" ><i class="fab fa-google-plus-g"></i></a></li>
                  <li class="row-end"><a href="#" ><i class="fas fa-rss"></i></a></li>
              </ul><!--//social-icons-->
              <form class="col-md-6 col-12 search-form" role="search">
                  <div class="form-group">
                      <input type="text" class="form-control" placeholder="Search the site...">
                  </div>
                  <button type="submit" class="btn btn-theme">Go</button>
              </form>
          </div><!--//row--> --}}
        </div>
    </div><!--//to-bar-->
    <div class="header-main container">
      <div class="row">
          <h1 class="logo col-md-4 col-12">
              <a href="index.html"><img id="logo" src="{{ config('app.url') }}/assets/images/logo_new.png" alt="Logo"></a>
          </h1><!--//logo-->
          <div class="info col-md-8 col-12">
              <ul class="menu-top float-right d-none d-md-block">
                  <li class="divider"><a href="{{config('app.url')}}/">Home</a></li>
                  <li class="divider"><a href="#">FAQ</a></li>
                  <li><a href="{{config('app.url')}}/contact">Contact</a></li>
              </ul><!--//menu-top-->
              <br />
              <div class="contact float-right">
                  <p class="phone"><i class="fas fa-phone"></i>08562 200038</p>
                  <p class="email"><i class="fas fa-envelope"></i><a href="#">directorrimskadapa@yahoo.com</a></p>
              </div><!--//contact-->
          </div><!--//info-->
      </div><!--//row-->
    </div><!--//header-main-->
</header>
