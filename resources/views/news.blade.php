@php
  use App\Newese;
@endphp
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themes.3rdwavemedia.com/college-green/bs4/3.0/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Oct 2018 10:44:10 GMT -->
<head>
    <title>News | RIMS Kadapa</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    <!-- FontAwesome JS-->
    <script defer src="{{ config('app.url') }}/use.fontawesome.com/releases/v5.1.0/js/all.js" integrity="sha384-3LK/3kTpDE/Pkp8gTNp2gR/2gOiwQ6QaO7Td0zV76UFJVhqLl4Vl3KL1We6q6wR9" crossorigin="anonymous"></script>
    <!-- Global CSS -->
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/plugins/bootstrap/css/bootstrap.min.css">
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/plugins/flexslider/flexslider.css">
    <!-- Theme CSS -->
    <link id="theme-style" rel="stylesheet" href="{{ config('app.url') }}/assets/css/theme-1.css">

</head>

<body>
    <div class="wrapper">
        <!-- ******HEADER****** -->
        @include('frontend.header')
        <!--//header-->

        <!-- ******NAV****** -->
        <!--//main-nav-container-->
        @php
          $page_id = 'news';
        @endphp

        @php
          $new = Newese::all();
        @endphp

        @include('frontend.navbar')
        <!-- ******CONTENT****** -->

        <div class="content container">
            <div class="page-wrapper">
                <header class="page-heading clearfix">
                    <h1 class="heading-title float-left">News</h1>
                    <div class="breadcrumbs float-right">
                        <ul class="breadcrumbs-list">
                            <li class="breadcrumbs-label">You are here:</li>
                            <li><a href="/">Home</a><i class="fas fa-angle-right"></i></li>
                            {{-- <li class="current">About</li> --}}
                            <li>
                            <?php $link = "" ?>
            @for($i = 1; $i <= count(Request::segments()); $i++)
             @if($i < count(Request::segments()) & $i > 0)
           <?php $link .= "/" . Request::segment($i); ?>
          <a href="<?= $link ?>">{{ ucwords(str_replace('-',' ',Request::segment($i)))}}</a>
             @else {{ucwords(str_replace('-',' ',Request::segment($i)))}}
         @endif
         @endfor
         </li>
                        </ul>
                    </div><!--//breadcrumbs-->
                </header>
                <div class="page-content">
                  <div class="row page-row">
                      <div class="news-wrapper col-lg-8 col-md-7">
                        @foreach ($new as $key)
                          <article class="news-item page-row has-divider clearfix row">
                              <figure class="thumb col-lg-2 col-md-3 col-4">
                                  <img class="img-fluid" style="height:100px;width:100px;"src="{{ config('app.url') }}/store/{{$key->image}}" alt="" />
                              </figure>
                              <div class="details col-lg-10 col-md-9 col-8">
                                  <h3 class="title"><a href="news-single.html">{{$key->heading}}</a></h3>
                                  <p>{!!substr($key->content, 0, 300)!!}</p>
                                  <a class="btn btn-theme read-more" href="{{config('app.url')}}/news-view/{{$key->id}}">Read more<i class="fas fa-chevron-right"></i></a>
                              </div>
                          </article><!--//news-item-->
                        @endforeach
                          <!--//news-item-->
                          <!--//news-item-->
                          <!--//news-item-->
                          <!--//news-item-->
                          <!--//news-item-->

                          {{-- <nav class="pagination-container text-center">
                          <ul class="pagination">
                              <li class="page-item disabled">
                                  <a class="page-link" href="#" arial-label="previous">
                                      <span aria-hidden="true">&laquo;</span>
                                      <span class="sr-only">Previous</span>
                                  </a>
                              </li>
                              <li class="page-item active"><a class="page-link" href="#">1<span class="sr-only">(current)</span></a></li>
                              <li class="page-item"><a class="page-link" href="#">2</a></li>
                              <li class="page-item"><a class="page-link" href="#">3</a></li>
                              <li class="page-item"><a class="page-link" href="#">4</a></li>
                              <li class="page-item"><a class="page-link" href="#">5</a></li>
                              <li class="page-item">
                                  <a class="page-link" href="#">
                                      <span aria-hidden="true">&raquo;</span>
                                      <span class="sr-only">Next</span>
                                  </a>
                              </li>
                          </ul><!--//pagination-->
                      </nav> --}}

                      </div><!--//news-wrapper-->
                      {{-- <aside class="page-sidebar  col-lg-3 offset-lg-1 col-md-4 offset-md-1">
                          <section class="widget has-divider">
                              <h3 class="title">Arcu Aliquet Quam Vel</h3>
                              <p>Maecenas nisl urna, condimentum ac justo a, adipiscing hendrerit magna. Fusce pharetra laoreet accumsan. Phasellus elit sapien, consequat vel sapien sit amet, condimentum vulputate odio. Aliquam fringilla justo quis est placerat, eu imperdiet lorem cursus. Curabitur pretium nulla lorem, sed egestas ante vestibulum dignissim.</p>
                          </section><!--//widget-->
                          <section class="widget has-divider">
                              <h3 class="title">Upcoming Events</h3>
                              <article class="events-item row page-row">
                                      <div class="date-label-wrapper col-lg-3 col-4">
                                          <p class="date-label">
                                              <span class="month">FEB</span>
                                              <span class="date-number">18</span>
                                          </p>
                                      </div><!--//date-label-wrapper-->
                                      <div class="details col-lg-9 col-8">
                                          <h5 class="title">Open Day</h5>
                                          <p class="time text-muted">10:00am - 18:00pm<br />East Campus</p>
                                      </div><!--//details-->
                              </article>
                              <article class="events-item row page-row">
                                  <div class="date-label-wrapper col-lg-3 col-4">
                                      <p class="date-label">
                                          <span class="month">SEP</span>
                                          <span class="date-number">06</span>
                                      </p>
                                  </div><!--//date-label-wrapper-->
                                  <div class="details col-lg-9 col-8">
                                      <h5 class="title">E-learning at College Green</h5>
                                      <p class="time text-muted">10:00am - 16:00pm<br />Learning Center</p>
                                  </div><!--//details-->
                              </article>
                              <article class="events-item row page-row">
                                  <div class="date-label-wrapper col-lg-3 col-4">
                                      <p class="date-label">
                                          <span class="month">JUN</span>
                                          <span class="date-number">23</span>
                                      </p>
                                  </div><!--//date-label-wrapper-->
                                  <div class="details col-lg-9 col-8">
                                      <h5 class="title">Career Fair</h5>
                                      <p class="time text-muted">09:45am - 16:00pm<br />Library</p>
                                  </div><!--//details-->
                              </article>
                          </section><!--//widget-->
                          <section class="widget">
                              <h3 class="title">Flickr Photo Stream</h3>
                              <ul id="flickr-photos"></ul><!--//flickr-photos-->
                          </section><!--//widget-->
                      </aside> --}}
                  </div><!--//page-row-->
                </div><!--//page-content-->
            </div><!--//page-->
        </div>
      </div>

    <!-- ******FOOTER****** -->
    @include('frontend.footer')
    <!--//footer-->

    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/popper.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/back-to-top.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/flexslider/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/jflickrfeed/jflickrfeed.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/js/main.js"></script>

    <!-- Theme Switcher (REMOVE ON YOUR PRODUCTION SITE) -->
    <script type="text/javascript" src="{{ config('app.url') }}/assets/js/demo/theme-switcher.js"></script>

</body>

<!-- Mirrored from themes.3rdwavemedia.com/college-green/bs4/3.0/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Oct 2018 10:44:29 GMT -->
</html>
