@php
  use App\GalleryEvent;
  use App\GalleryImage;
@endphp
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themes.3rdwavemedia.com/college-green/bs4/3.0/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Oct 2018 10:44:10 GMT -->
<head>
    <title>Gallery View | RIMS Kadapa</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    <!-- FontAwesome JS-->
    <script defer src="{{ config('app.url') }}/use.fontawesome.com/releases/v5.1.0/js/all.js" integrity="sha384-3LK/3kTpDE/Pkp8gTNp2gR/2gOiwQ6QaO7Td0zV76UFJVhqLl4Vl3KL1We6q6wR9" crossorigin="anonymous"></script>
    <!-- Global CSS -->
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/plugins/bootstrap/css/bootstrap.min.css">
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/plugins/flexslider/flexslider.css">


    <link rel="stylesheet" href="{{ config('app.url') }}/assets/css/lightbox.css">

    <link rel="stylesheet" href="{{ config('app.url') }}/assets/css/lightbox.min.css">

    <!-- Theme CSS -->
    <link id="theme-style" rel="stylesheet" href="{{ config('app.url') }}/assets/css/theme-1.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">

</head>

<body>
    <div class="wrapper">
        <!-- ******HEADER****** -->
        @include('frontend.header')
        <!--//header-->

        <!-- ******NAV****** -->
        <!--//main-nav-container-->
        @php
          $page_id = 'gallery';
        @endphp

        {{-- @php
          $new = GalleryEvent::all();
        @endphp --}}

        @include('frontend.navbar')
        <!-- ******CONTENT****** -->

    @php
     $view = $key;
     $new = DB::select("SELECT * FROM gallery_images WHERE event_title ='$view'");
   @endphp

        <div class="content container">
            <div class="page-wrapper">
                <header class="page-heading clearfix">
                  @php
                    // $data = $key->event_title;
                    $details = GalleryEvent::find($key);
                  @endphp
                    <h1 class="heading-title float-left">{{$details['title']}}</h1>
                    <div class="breadcrumbs float-right">
                        <ul class="breadcrumbs-list">
                            <li class="breadcrumbs-label">You are here:</li>
                            <li><a href="/">Home</a><i class="fas fa-angle-right"></i></li>
                            {{-- <li class="current">About</li> --}}
                            <li>
                            <?php $link = "" ?>
            @for($i = 1; $i <= count(Request::segments()); $i++)
             @if($i < count(Request::segments()) & $i > 0)
           <?php $link .= "/" . Request::segment($i); ?>
          <a href="<?= $link ?>">{{ ucwords(str_replace('-',' ',Request::segment($i)))}}</a>
             @else {{ucwords(str_replace('-',' ',Request::segment($i)))}}
         @endif
         @endfor
         </li>
                        </ul>
                    </div><!--//breadcrumbs-->
                </header>
                <div class="page-content">
                    <div id="gallery-album" class="gallery-album row page-row">
                      @foreach ($new as $key)
                        @php
                          $project_id = $key->event_title;
                          $details = GalleryEvent::find($project_id);
                        @endphp
                        <div class="item col-md-3 col-6" title="{{$details->title}}">
                            <a href="{{ config('app.url') }}/store/{{$key->image}}"  title="{{$details->title}}"data-lightbox="roadtrip">
                              <img class="img-fluid" src="{{ config('app.url') }}/store/{{$key->image}}" alt="No Image">
                            </a>
	                    </div><!--//item-->
                    @endforeach
                    </div><!--//page-row-->
                </div><!--//page-content-->
            </div><!--//page-->
        </div>
      </div>

    <!-- ******FOOTER****** -->
    @include('frontend.footer')
    <!--//footer-->

    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/popper.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/back-to-top.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/flexslider/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/jflickrfeed/jflickrfeed.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/js/main.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/js/lightbox.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/js/lightbox-plus-jquery.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/js/lightbox-plus-jquery.min.js"></script>

    <!-- Theme Switcher (REMOVE ON YOUR PRODUCTION SITE) -->
    <script type="text/javascript" src="{{ config('app.url') }}/assets/js/demo/theme-switcher.js"></script>

</body>

<!-- Mirrored from themes.3rdwavemedia.com/college-green/bs4/3.0/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Oct 2018 10:44:29 GMT -->
</html>
