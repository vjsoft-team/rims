{{-- @php
  use App\Banner;
@endphp --}}
@php
  // $page_id = $content[0]['slug'];
  // dd($page_id);
@endphp
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themes.3rdwavemedia.com/college-green/bs4/3.0/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Oct 2018 10:44:10 GMT -->
<head>
    <title>Courses | RIMS Kadapa</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    <!-- FontAwesome JS-->
    <script defer src="{{ config('app.url') }}/use.fontawesome.com/releases/v5.1.0/js/all.js" integrity="sha384-3LK/3kTpDE/Pkp8gTNp2gR/2gOiwQ6QaO7Td0zV76UFJVhqLl4Vl3KL1We6q6wR9" crossorigin="anonymous"></script>
    <!-- Global CSS -->
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/plugins/bootstrap/css/bootstrap.min.css">
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/plugins/flexslider/flexslider.css">
    <!-- Theme CSS -->
    <link id="theme-style" rel="stylesheet" href="{{ config('app.url') }}/assets/css/theme-1.css">

</head>

<body class="home-page">
    <div class="wrapper">
        <!-- ******HEADER****** -->
        @include('frontend.header')
        <!--//header-->

        <!-- ******NAV****** -->
        <!--//main-nav-container-->
        @php

          $page_id = $content[0]['slug'];
          $page_id = DB::SELECT("SELECT * FROM menu_items WHERE url = '$page_id'");
          // dd($page_id[0]->parent_id);
          $page_id = $page_id[0]->parent_id;
        @endphp
        @include('frontend.navbar')
        <!-- ******CONTENT****** -->
        <div class="content container">
          <div class="page-wrapper">
              <header class="page-heading clearfix">
                @foreach ($content as $key )

                  <h1 class="heading-title float-left">{{$key->title}}</h1>
                  <div class="breadcrumbs float-right">
                      <ul class="breadcrumbs-list">
                          <li class="breadcrumbs-label">You are here:</li>
                          <li><a href="/">Home</a><i class="fas fa-angle-right"></i></li>
                          <li><a href="#">Course</a><i class="fas fa-angle-right"></i></li>
                          <li>
                          <?php $link = "" ?>
          @for($i = 1; $i <= count(Request::segments()); $i++)
           @if($i < count(Request::segments()) & $i > 0)
         <?php $link .= "/" . Request::segment($i); ?>
        <a href="<?= $link ?>">{{ ucwords(str_replace('-',' ',Request::segment($i)))}}</a>
           @else {{ucwords(str_replace('-',' ',Request::segment($i)))}}
       @endif
       @endfor
                </li>          {{-- <li class="current">Web Design Foundation</li> --}}
                      </ul>
                  </div><!--//breadcrumbs-->
              </header>
              <div class="page-content">
                  <div class="row page-row">
                      <div class="course-wrapper col-lg-8 col-md-7">
                          <article class="course-item">
                              <p class="featured-image page-row"><img class="img-fluid" style="width:688px; height:269px;" src="{{ config('app.url') }}/store/{{$key->image}}" alt=""/></p>
                              {{-- <div class="page-row box box-border">
                                  <ul class="list-unstyled no-margin-bottom">
                                      <li><strong>Start date:</strong> <em>24 Sep 2014</em></li>
                                      <li><strong>Duration: </strong> <em>1 year</em></li>
                                      <li><strong>Level: </strong> <em>Beginner</em></li>
                                      <li><strong>Location: </strong> <em>Remote(Online)</em></li>
                                  </ul>
                              </div><!--//page-row--> --}}
                              <div class="page-row">
                                  <p>{!!$key->body!!}</p>
                              <div class="tabbed-info page-row">
                                  <ul class="nav nav-tabs">
                                    <li class="nav-item"><a class="nav-link active" href="#tab1" data-toggle="tab">Course structure</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#tab2" data-toggle="tab">Fees</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#tab3" data-toggle="tab">Entry requirements</a></li>
                                  </ul>
                                  <div class="tab-content">
                                      <div class="tab-pane active" id="tab1">
                                          <p>{{$key->course_structure}}</p>
                                      {{-- <div class="table-responsive">
                                          <table class="table table-striped">
                                              <thead>
                                                  <tr>
                                                      <th>Nullam consequat</th>
                                                      <th>Commodo metus</th>
                                                      <th>Dapibus porta</th>
                                                      <th>Sed porta</th>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                  <tr>
                                                      <td>Faucibus purus convallis</td>
                                                      <td>Aliquam sit amet</td>
                                                      <td>Sed porta leo</td>
                                                      <td>Duis ut ornare dui</td>
                                                  </tr>
                                                  <tr>
                                                      <td>Condimentum fermentum</td>
                                                      <td>Curabitur tempus mauris</td>
                                                      <td>Fusce vehicula malesuada</td>
                                                      <td>Nascetur ridiculus</td>
                                                  </tr>
                                                  <tr>
                                                      <td>Neque a condimentum</td>
                                                      <td>Cum sociis natoque</td>
                                                      <td>Penatibus magnis</td>
                                                      <td>Curabitur tempus mauris</td>
                                                  </tr>
                                              </tbody>
                                          </table>
                                      </div><!--//table-responsive--> --}}
                                      </div>
                                      <div class="tab-pane" id="tab2">
                                          <p>{{$key->fees}}</p>

                                      </div>
                                      <div class="tab-pane" id="tab3">
                                          <p>{{$key->faculty}}</p>

                                      </div>
                                  </div>
                                @endforeach
                              </div><!--//tabbed-info-->
                          </article><!--//course-item-->
                      </div><!--//course-wrapper-->
                      <aside class="page-sidebar  col-lg-3 offset-lg-1 col-md-4 offset-md-1">
                          <section class="widget has-divider">
                              <h3 class="title">Why College Green Online</h3>
                              <p>Donec pulvinar arcu lacus, vel aliquam libero scelerisque a. Cras mi tellus, vulputate eu eleifend at, consectetur fringilla lacus. Nulla ut purus.</p>
                              <a class="btn btn-theme" href="#"><i class="fas fa-download"></i>Download prospectus</a>
                          </section><!--//widget-->
                          <section class="widget has-divider">
                              <h3 class="title">Enquire about this course</h3>
                              <p>Donec pulvinar arcu lacus, vel aliquam libero scelerisque a. Cras mi tellus, vulputate eu eleifend at, consectetur fringilla lacus. Nulla ut purus.</p>
                              <p class="tel"><i class="fas fa-phone"></i>Tel: 0800 123 4567</p>
                              <p class="email"><i class="fas fa-envelope"></i>Email: <a href="#">enquires@website.com</a></p>
                          </section><!--//widget-->
                          {{-- <section class="widget has-divider">
                              <h3 class="title">Related courses</h3>
                              <ul class="list-unstyled">
                                  <li><a href="#"><i class="fas fa-book"></i> Ut enim ad minim veniam</a></li>
                                  <li><a href="#"><i class="fas fa-book"></i> Lorem ipsum dolor sit amet</a></li>
                                  <li><a href="#"><i class="fas fa-book"></i> Cras et sapien rhoncus</a></li>
                                  <li><a href="#"><i class="fas fa-book"></i> Praesent ut turpis feugiat</a></li>
                                  <li><a href="#"><i class="fas fa-book"></i> Aenean interdum iaculis odio</a></li>
                                  <li><a href="#"><i class="fas fa-book"></i> Morbi in malesuada nibh</a></li>
                                  <li><a href="#"><i class="fas fa-book"></i> Fusce a ligula in velit</a></li>
                              </ul>
                          </section><!--//widget--> --}}
                          {{-- <section class="widget has-divider">
                              <h3 class="title">Apply Online</h3>
                              <p>Donec pulvinar arcu lacus, vel aliquam libero scelerisque a. Cras mi tellus, vulputate eu eleifend at, consectetur fringilla lacus. Nulla ut purus.</p>
                              <p class="promo-badge">
                                  <a class="OliveDrab" href="#">
                                      <span class="percentage">20% <span class="off">OFF</span></span>
                                      <br>
                                      <span class="desc">Online application</span>
                                  </a>
                              </p>
                          </section><!--//widget--> --}}
                      </aside>
                  </div><!--//page-row-->
              </div><!--//page-content-->
          </div>
            <!--//flexslider-->
            <!--//promo-->
            <!--//news-->
            <!--//cols-wrapper-->

        </div><!--//content-->
    </div><!--//wrapper-->

    <!-- ******FOOTER****** -->
    @include('frontend.footer')
    <!--//footer-->

    <!-- *****CONFIGURE STYLE****** -->
    {{-- <div class="config-wrapper d-none d-md-block">
        <div class="config-wrapper-inner">
            <a id="config-trigger" class="config-trigger" href="#"><i class="fa fa-cog mx-auto"></i></a>
            <div id="config-panel" class="config-panel">
                <p>Choose Colour</p>
                <ul id="color-options" class="list-unstyled list-inline">
                    <li class="list-inline-item default active" ><a data-style="{{ config('app.url') }}/assets/css/theme-1.css" data-logo="{{ config('app.url') }}/assets/images/logo.png" href="#"></a></li>
                    <li class="list-inline-item green"><a data-style="{{ config('app.url') }}/assets/css/theme-2.css" data-logo="{{ config('app.url') }}/assets/images/logo-green.png" href="#"></a></li>
                    <li class="list-inline-item purple"><a data-style="{{ config('app.url') }}/assets/css/theme-3.css" data-logo="{{ config('app.url') }}/assets/images/logo-purple.png" href="#"></a></li>
                    <li class="list-inline-item red"><a data-style="{{ config('app.url') }}/assets/css/theme-4.css" data-logo="{{ config('app.url') }}/assets/images/logo-red.png" href="#"></a></li>
                </ul><!--//color-options-->
                <a id="config-close" class="close" href="#"><i class="fa fa-times-circle"></i></a>
            </div><!--//configure-panel-->
        </div><!--//config-wrapper-inner-->
    </div><!--//config-wrapper--> --}}

    <!-- Javascript -->
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/popper.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/back-to-top.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/flexslider/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/jflickrfeed/jflickrfeed.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/js/main.js"></script>

    <!-- Theme Switcher (REMOVE ON YOUR PRODUCTION SITE) -->
    <script type="text/javascript" src="{{ config('app.url') }}/assets/js/demo/theme-switcher.js"></script>

</body>

<!-- Mirrored from themes.3rdwavemedia.com/college-green/bs4/3.0/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Oct 2018 10:44:29 GMT -->
</html>
