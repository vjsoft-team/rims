@php
  use App\Banner;
  use App\Newese;
@endphp
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Rajiv Gandhi Institute of Medical Sciences - Home</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    <!-- FontAwesome JS-->
    <script defer src="{{ config('app.url') }}/use.fontawesome.com/releases/v5.1.0/js/all.js" integrity="sha384-3LK/3kTpDE/Pkp8gTNp2gR/2gOiwQ6QaO7Td0zV76UFJVhqLl4Vl3KL1We6q6wR9" crossorigin="anonymous"></script>
    <!-- Global CSS -->
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/plugins/bootstrap/css/bootstrap.min.css">
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/plugins/flexslider/flexslider.css">
    <!-- Theme CSS -->
    <link id="theme-style" rel="stylesheet" href="{{ config('app.url') }}/assets/css/theme-1.css">

</head>

<body class="home-page">
    <div class="wrapper">
        <!-- ******HEADER****** -->
        @include('frontend.header')
        <!--//header-->

        <!-- ******NAV****** -->
        <!--//main-nav-container-->
        @php

          $page_id = '/';
        @endphp
        @include('frontend.navbar')
        <!-- ******CONTENT****** -->
        @php
          $new = Banner::all();
        @endphp
        <div class="content container">
            <div id="promo-slider" class="slider flexslider">
                <ul class="slides">
                  @foreach ($new as $key)
                    <li>
                        <img src="{{ config('app.url') }}/store/{{$key->slider}}"  alt="No image" />
                        <p class="flex-caption">
                            <span class="main" >{{$key->heading}}</span>
                            <br />
                            <span class="secondary clearfix" >{{$key->sub_heading}}</span>
                        </p>
                    </li>
                  @endforeach
                </ul><!--//slides-->
            </div><!--//flexslider-->

            @php
              $news = Newese::limit(3)->offset(0)->get();
              $test = Newese::limit(3)->offset(3)->get();
            @endphp

            <section class="news">
                <h1 class="section-heading text-highlight"><span class="line">Latest News</span></h1>
                <div class="carousel-controls">
                    <a class="prev" href="#news-carousel" data-slide="prev"><i class="fas fa-caret-left"></i></a>
                    <a class="next" href="#news-carousel" data-slide="next"><i class="fas fa-caret-right"></i></a>
                </div><!--//carousel-controls-->
                <div class="section-content clearfix">
                    <div id="news-carousel" class="news-carousel carousel slide">
                        <div class="carousel-inner">
                            <div class="item carousel-item active">
	                            <div class="row">
                                @foreach ($news as $key)
	                                <div class="col-lg-4 col-12 news-item">

	                                    <h2 class="title"><a href="#">{{$key->heading}}</a></h2>
	                                    <img class="thumb" style="height:100px;width:100px;" src="{{ config('app.url') }}/store/{{$key->image}}"  alt="" />
	                                    <p>{!!substr($key->content, 0, 200)!!}</p>
	                                    <a class="read-more" href="{{config('app.url')}}/news-view/{{$key->id}}">Read more<i class="fas fa-chevron-right"></i></a>
	                                </div><!--//news-item-->
                                @endforeach
	                            </div><!--//row-->
                            </div><!--//item-->
                            <div class="item carousel-item">
	                            <div class="row">
                                @foreach ($test as $key)
	                                <div class="col-lg-4 col-12 news-item">

	                                    <h2 class="title"><a href="#">{{$key->heading}}</a></h2>
	                                    <img class="thumb" style="height:100px;width:100px;" src="{{ config('app.url') }}/store/{{$key->image}}"  alt="" />
	                                    <p>{!!substr($key->content, 0, 200)!!}</p>
	                                    <a class="read-more" href="{{config('app.url')}}/news/{{$key->id}}">Read more<i class="fas fa-chevron-right"></i></a>
	                                </div><!--//news-item-->
                                @endforeach
	                            </div><!--//row-->
                            </div><!--//item-->
                            {{-- <div class="item carousel-item">
	                            <div class="row">
	                                <div class="col-lg-4 col-12 news-item">
	                                    <h2 class="title"><a href="#">Phasellus scelerisque metus</a></h2>
	                                    <img class="thumb" src="{{ config('app.url') }}/assets/images/news/news-thumb-4.jpg"  alt="" />
	                                    <p>Suspendisse purus felis, porttitor quis sollicitudin sit amet, elementum et tortor. Praesent lacinia magna in malesuada vestibulum. Pellentesque urna libero.</p>
	                                    <a class="read-more" href="#">Read more<i class="fas fa-chevron-right"></i></a>
	                                </div><!--//news-item-->
	                                <div class="col-lg-4 col-12 news-item">
	                                    <h2 class="title"><a href="#">Morbi at vestibulum turpis</a></h2>
	                                    <p>Nam feugiat erat vel neque mollis, non vulputate erat aliquet. Maecenas ac leo porttitor, semper risus condimentum, cursus elit. Vivamus vitae libero tellus.</p>
	                                    <a class="read-more" href="#">Read more<i class="fas fa-chevron-right"></i></a>
	                                    <img class="thumb" src="{{ config('app.url') }}/assets/images/news/news-thumb-5.jpg"  alt="" />
	                                </div><!--//news-item-->
	                                <div class="col-lg-4 col-12 news-item">
	                                    <h2 class="title"><a href="#">Aliquam id iaculis urna</a></h2>
	                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam bibendum mauris eget sapien consectetur pellentesque. Proin elementum tristique euismod. </p>
	                                    <a class="read-more" href="#">Read more<i class="fas fa-chevron-right"></i></a>
	                                    <img class="thumb" src="{{ config('app.url') }}/assets/images/news/news-thumb-6.jpg"  alt="" />
	                                </div><!--//news-item-->
	                            </div><!--//row-->
                            </div><!--//item--> --}}
                        </div><!--//carousel-inner-->
                    </div><!--//news-carousel-->
                </div><!--//section-content-->
            </section><!--//news-->

            {{-- <article class="welcome col-lg-8 col-md-7 col-12">
                <h3 class="title">Welcome to RIMS</h3>
                <p><img class="img-fluid" style="width:750px;height:300px" src="assets/images/about.jpg" alt="" /></p>

                  <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, </p>

            </article> --}}

            <div class="row cols-wrapper">
                <div class="col-lg-3 col-md-6 col-12">
                    <section class="events" style="width:806px;">
                        {{-- <h1 class="section-heading text-highlight"><span class="line">Events</span></h1> --}}
                        <h3 class="title">Welcome to RIMS</h3>
                        <p><img class="img-fluid" style="width:750px;height:300px" src="assets/images/about.jpg" alt="" /></p>

                          <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, </p>
                        {{-- <div class="section-content">
                            <div class="event-item">
                                <p class="date-label">
                                    <span class="month">FEB</span>
                                    <span class="date-number">18</span>
                                </p>
                                <div class="details">
                                    <h2 class="title">Open Day</h2>
                                    <p class="time"><i class="far fa-clock"></i>10:00am - 18:00pm</p>
                                    <p class="location"><i class="fas fa-map-marker-alt"></i>East Campus</p>
                                </div><!--//details-->
                            </div><!--event-item-->
                            <div class="event-item">
                                <p class="date-label">
                                    <span class="month">SEP</span>
                                    <span class="date-number">06</span>
                                </p>
                                <div class="details">
                                    <h2 class="title">E-learning at College Green</h2>
                                    <p class="time"><i class="far fa-clock"></i>10:00am - 16:00pm</p>
                                    <p class="location"><i class="fas fa-map-marker-alt"></i>Learning Center</p>
                                </div><!--//details-->
                            </div><!--event-item-->
                            <div class="event-item">
                                <p class="date-label">
                                    <span class="month">JUN</span>
                                    <span class="date-number">23</span>
                                </p>
                                <div class="details">
                                    <h2 class="title">Career Fair</h2>
                                    <p class="time"><i class="far fa-clock"></i>09:45am - 16:00pm</p>
                                    <p class="location"><i class="fas fa-map-marker-alt"></i>Library</p>
                                </div><!--//details-->
                            </div><!--event-item-->
                            <div class="event-item">
                                <p class="date-label">
                                    <span class="month">May</span>
                                    <span class="date-number">17</span>
                                </p>
                                <div class="details">
                                    <h2 class="title">Science Seminar</h2>
                                    <p class="time"><i class="far fa-clock"></i>14:00pm - 18:00pm</p>
                                    <p class="location"><i class="fas fa-map-marker-alt"></i>Library</p>
                                </div><!--//details-->
                            </div><!--event-item-->
                            <a class="read-more" href="#">All events<i class="fas fa-chevron-right"></i></a>
                        </div><!--//section-content--> --}}
                    </section><!--//events-->
                </div><!--//col-md-3-->
                <div class="col-lg-6 col-12">
                    {{-- <section class="course-finder">
                        <h1 class="section-heading text-highlight"><span class="line">Course Finder</span></h1>
                        <div class="section-content">
                            <form class="course-finder-form" action="#" method="get">
                                <div class="form-row">
                                    <div class="col-md-5 col-12 subject">
                                        <select class="form-control subject">
                                            <option>Choose a subject area</option>
                                            <option>Accounting & Finance</option>
                                            <option>Biological Sciences</option>
                                            <option>Business Studies</option>
                                            <option>Computer Science</option>
                                            <option>Creative Arts & Media</option>
                                            <option>Drama</option>
                                            <option>Education</option>
                                            <option>Engineering</option>
                                            <option>Film Studies</option>
                                            <option>Fitness Training</option>
                                            <option>Hospitality</option>
                                            <option>History</option>
                                            <option>International Relations</option>
                                            <option>Law</option>
                                            <option>Mathematics</option>
                                            <option>Music</option>
                                            <option>Physics</option>
                                            <option>Religion</option>
                                            <option>Social Science</option>
                                        </select>
                                    </div>
                                    <div class="col-md-7 col-12 keywords">
                                        <input class="form-control float-left" type="text" placeholder="Search keywords" />
                                        <button type="submit" class="btn btn-theme"><i class="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </form><!--//course-finder-form-->
                        </div><!--//section-content-->
                    </section><!--//course-finder--> --}}
                    <section class="video">
                        {{-- <h1 class="section-heading text-highlight"><span class="line">Video Tour</span></h1> --}}
                        {{-- <div class="carousel-controls">
                            <a class="prev" href="#videos-carousel" data-slide="prev"><i class="fas fa-caret-left"></i></a>
                            <a class="next" href="#videos-carousel" data-slide="next"><i class="fas fa-caret-right"></i></a>
                        </div><!--//carousel-controls--> --}}
                        {{-- <div class="section-content">
                           <div id="videos-carousel" class="videos-carousel carousel slide">
                                <div class="carousel-inner">
                                    <div class="carousel-item item active">
	                                    <div class="embed-responsive embed-responsive-16by9 mb-2">
	                                        <iframe class="embed-responsive-item" src="http://www.youtube.com/embed/r9LelXa3U_I?rel=0&amp;wmode=transparent" frameborder="0" allowfullscreen=""></iframe>
	                                    </div>
                                    </div><!--//item-->
                                    <div class="carousel-item item">
                                        <div class="embed-responsive embed-responsive-16by9 mb-2">
	                                        <iframe class="embed-responsive-item" src="http://www.youtube.com/embed/RcGyVTAoXEU?rel=0&amp;wmode=transparent" frameborder="0" allowfullscreen=""></iframe>
	                                    </div>
                                    </div><!--//item-->
                                    <div class="carousel-item item">
                                        <div class="embed-responsive embed-responsive-16by9 mb-2">
	                                        <iframe class="embed-responsive-item" src="http://www.youtube.com/embed/Ks-_Mh1QhMc?rel=0&amp;wmode=transparent" frameborder="0" allowfullscreen=""></iframe>
	                                    </div>
                                    </div><!--//item-->
                                </div><!--//carousel-inner-->
                           </div><!--//videos-carousel-->
                            <p class="description">Aenean feugiat a diam tempus sodales. Quisque lorem nulla, ultrices imperdiet malesuada at, suscipit vel lorem. Nulla dignissim nisi ac aliquet semper.</p>
                        </div><!--//section-content--> --}}
                    </section><!--//video-->
                </div>
                <div class="col-lg-3 col-12" style="top:47px;">
                    <section class="links">
                        <h1 class="section-heading text-highlight"><span class="line">Quick Links</span></h1>
                        <div class="section-content">
                            <p><a href="#"><i class="fas fa-caret-right"></i>About us</a></p>
                            <p><a href="#"><i class="fas fa-caret-right"></i>Gallery</a></p>
                            <p><a href="#"><i class="fas fa-caret-right"></i>News</a></p>
                            <p><a href="#"><i class="fas fa-caret-right"></i>UG Academics</a></p>
                            <p><a href="#"><i class="fas fa-caret-right"></i>PG Academics</a></p>
                            <p><a href="#"><i class="fas fa-caret-right"></i>Contact</a></p>
                        </div><!--//section-content-->
                    </section><!--//links-->
                    {{-- <section class="testimonials">
                        <h1 class="section-heading text-highlight"><span class="line"> Testimonials</span></h1>
                        <div class="carousel-controls">
                            <a class="prev" href="#testimonials-carousel" data-slide="prev"><i class="fas fa-caret-left"></i></a>
                            <a class="next" href="#testimonials-carousel" data-slide="next"><i class="fas fa-caret-right"></i></a>
                        </div><!--//carousel-controls-->
                        <div class="section-content">
                            <div id="testimonials-carousel" class="testimonials-carousel carousel slide">
                                <div class="carousel-inner">
                                    <div class="carousel-item item active">
                                        <blockquote class="quote">
                                            <p><i class="fas fa-quote-left"></i>I’m very happy interdum eget ipsum. Nunc pulvinar ut nulla eget sollicitudin. In hac habitasse platea dictumst. Integer mattis varius ipsum, posuere posuere est porta vel. Integer metus ligula, blandit ut fermentum a, rhoncus in ligula. Duis luctus.</p>
                                        </blockquote>
                                        <div class="source">
                                            <p class="people"><span class="name">Marissa Spencer</span><br /><span class="title">Curabitur commodo</span></p>
                                            <img class="profile" src="{{ config('app.url') }}/assets/images/testimonials/profile-1.png"  alt="" />
                                        </div>
                                    </div><!--//item-->
                                    <div class="carousel-item item">
                                        <blockquote class="quote">
                                            <p><i class="fas fa-quote-left"></i>
                                            I'm very pleased commodo gravida ultrices. Sed massa leo, aliquet non velit eu, volutpat vulputate odio. Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse porttitor metus eros, ut fringilla nulla auctor a.</p>
                                        </blockquote>
                                        <div class="source">
                                            <p class="people"><span class="name">Marco Antonio</span><br /><span class="title"> Gravida ultrices</span></p>
                                            <img class="profile" src="{{ config('app.url') }}/assets/images/testimonials/profile-2.png"  alt="" />
                                        </div>
                                    </div><!--//item-->
                                    <div class="carousel-item item">
                                        <blockquote class="quote">
                                            <p><i class="fas fa-quote-left"></i>
                                            I'm delighted commodo gravida ultrices. Sed massa leo, aliquet non velit eu, volutpat vulputate odio. Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse porttitor metus eros, ut fringilla nulla auctor a.</p>
                                        </blockquote>
                                        <div class="source">
                                            <p class="people"><span class="name">Kate White</span><br /><span class="title"> Gravida ultrices</span></p>
                                            <img class="profile" src="{{ config('app.url') }}/assets/images/testimonials/profile-3.png"  alt="" />
                                        </div>
                                    </div><!--//item-->

                                </div><!--//carousel-inner-->
                            </div><!--//testimonials-carousel-->
                        </div><!--//section-content-->
                    </section><!--//testimonials--> --}}
                </div><!--//col-md-3-->
            </div><!--//cols-wrapper-->
            {{-- <section class="awards">
                <div id="awards-carousel" class="awards-carousel carousel slide">
                    <div class="carousel-inner">
                        <div class="carousel-item item active">
                            <ul class="logos row">

                                <li class="col-md-2 col-4">
                                    <a href="#"><img class="img-fluid" style="height:112px;" src="{{ config('app.url') }}/assets/images/awards/award1.jpg"  alt="" /></a>
                                </li>
                                <li class="col-md-2 col-4">
                                    <a href="#"><img class="img-fluid" style="height:112px;" src="{{ config('app.url') }}/assets/images/awards/award2.jpg"  alt="" /></a>
                                </li>
                                <li class="col-md-2 col-4">
                                    <a href="#"><img class="img-fluid" style="height:112px;" src="{{ config('app.url') }}/assets/images/awards/award3.jpg"  alt="" /></a>
                                </li>
                                <li class="col-md-2 col-4">
                                    <a href="#"><img class="img-fluid" style="height:112px;" src="{{ config('app.url') }}/assets/images/awards/award4.jpg"  alt="" /></a>
                                </li>
                                <li class="col-md-2 col-4">
                                    <a href="#"><img class="img-fluid" style="height:112px;" src="{{ config('app.url') }}/assets/images/awards/award5.jpg"  alt="" /></a>
                                </li>
                                <li class="col-md-2 col-4">
                                    <a href="#"><img class="img-fluid" style="height:112px;" src="{{ config('app.url') }}/assets/images/awards/award6.jpg"  alt="" /></a>
                                </li>
                            </ul><!--//slides-->
                        </div><!--//item-->

                        <div class="carousel-item item">
                            <ul class="logos row">
                                <li class="col-md-2 col-4">
                                    <img class="img-fluid" style="height:112px;"src="{{ config('app.url') }}/assets/images/awards/award7.jpg"  alt="" />
                                </li>
                                <li class="col-md-2 col-4">
                                    <img class="img-fluid" style="height:112px;" src="{{ config('app.url') }}/assets/images/awards/award6.jpg"  alt="" />
                                </li>
                                <li class="col-md-2 col-4">
                                    <img class="img-fluid" style="height:112px;" src="{{ config('app.url') }}/assets/images/awards/award5.jpg"  alt="" />
                                </li>
                                <li class="col-md-2 col-4">
                                    <img class="img-fluid" style="height:112px;" src="{{ config('app.url') }}/assets/images/awards/award4.jpg"  alt="" />
                                </li>
                                <li class="col-md-2 col-4">
                                    <img class="img-fluid" style="height:112px;" src="{{ config('app.url') }}/assets/images/awards/award3.jpg"  alt="" />
                                </li>
                                <li class="col-md-2 col-4">
                                    <img class="img-fluid" style="height:112px;" src="{{ config('app.url') }}/assets/images/awards/award2.jpg"  alt="" />
                                </li>
                            </ul><!--//slides-->
                        </div><!--//item-->
                    </div><!--//carousel-inner-->
                    <a class="left carousel-control" href="#awards-carousel" data-slide="prev">
                        <i class="fas fa-angle-left"></i>
                    </a>
                    <a class="right carousel-control" href="#awards-carousel" data-slide="next">
                        <i class="fas fa-angle-right"></i>
                    </a>

                </div>
            </section> --}}
        </div><!--//content-->
    </div><!--//wrapper-->

    <!-- ******FOOTER****** -->
    @include('frontend.footer')
    <!--//footer-->


    <!-- Javascript -->
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/popper.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/back-to-top.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/flexslider/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/jflickrfeed/jflickrfeed.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/js/main.js"></script>


    <script type="text/javascript" src="{{ config('app.url') }}/assets/js/demo/theme-switcher.js"></script>

</body>


</html>
