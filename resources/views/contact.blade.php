
<html lang="en">

<head>
    <title>Contact Us | RIMS Kadapa</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    <!-- FontAwesome JS-->
    <script defer src="{{ config('app.url') }}/use.fontawesome.com/releases/v5.1.0/js/all.js" integrity="sha384-3LK/3kTpDE/Pkp8gTNp2gR/2gOiwQ6QaO7Td0zV76UFJVhqLl4Vl3KL1We6q6wR9" crossorigin="anonymous"></script>
    <!-- Global CSS -->
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/plugins/bootstrap/css/bootstrap.min.css">
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/plugins/flexslider/flexslider.css">
    <!-- Theme CSS -->
    <link id="theme-style" rel="stylesheet" href="{{ config('app.url') }}/assets/css/theme-1.css">

</head>

<body>
    <div class="wrapper">
        <!-- ******HEADER****** -->
        @include('frontend.header')
        <!--//header-->

        <!-- ******NAV****** -->
        <!--//main-nav-container-->
        @php
          $page_id = 'contact';
        @endphp

        @include('frontend.navbar')
        <!-- ******CONTENT****** -->

        <div class="content container">
            <div class="page-wrapper">
                <header class="page-heading clearfix">
                    <h1 class="heading-title float-left">Contact</h1>
                    <div class="breadcrumbs float-right">
                        <ul class="breadcrumbs-list">
                            <li class="breadcrumbs-label">You are here:</li>
                            <li><a href="/">Home</a><i class="fas fa-angle-right"></i></li>
                            {{-- <li class="current">About</li> --}}
                            <li>
                            <?php $link = "" ?>
            @for($i = 1; $i <= count(Request::segments()); $i++)
             @if($i < count(Request::segments()) & $i > 0)
           <?php $link .= "/" . Request::segment($i); ?>
          <a href="<?= $link ?>">{{ ucwords(str_replace('-',' ',Request::segment($i)))}}</a>
             @else {{ucwords(str_replace('-',' ',Request::segment($i)))}}
         @endif
         @endfor
         </li>
                        </ul>
                    </div><!--//breadcrumbs-->
                </header>
                <div class="page-content">
                  <div class="row">
                      <article class="contact-form col-lg-8 col-md-7  col-12 page-row">
                          <h3 class="title">Get in touch</h3>
                          <p>We’d love to hear from you. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ut erat magna. Aliquam porta sem a lacus imperdiet posuere. Integer semper eget ligula id eleifend. </p>
                          <article class="map-section">
                              <h3 class="title">How to find us</h3>
                              <div class="gmap-wrapper" style="padding-bottom:373px;" id="map">
                                  <!--//You need to embed your own google map below-->

                                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3863.8946221181377!2d78.86226121483824!3d14.433235989909212!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bb5ddc0d8ce48ab%3A0xbf2dea498a566ed5!2sRIMS+Kadapa!5e0!3m2!1sen!2sin!4v1541152562347" width="600" height="570" frameborder="0" style="border:0" allowfullscreen></iframe>
                              </div><!--//gmap-wrapper-->
                          </article>
                          {{-- <form>
                              <div class="form-group name">
                                  <label for="name">Name</label>
                                  <input id="name" type="text" class="form-control" placeholder="Enter your name">
                              </div><!--//form-group-->
                              <div class="form-group email">
                                  <label for="email">Email<span class="required">*</span></label>
                                  <input id="email" type="email" class="form-control" placeholder="Enter your email">
                              </div><!--//form-group-->
                              <div class="form-group phone">
                                  <label for="phone">Phone</label>
                                  <input id="phone" type="tel" class="form-control" placeholder="Enter your contact number">
                              </div><!--//form-group-->
                              <div class="form-group message">
                                  <label for="message">Message<span class="required">*</span></label>
                                  <textarea id="message" class="form-control" rows="6" placeholder="Enter your message here..."></textarea>
                              </div><!--//form-group-->
                              <button type="submit" class="btn btn-theme">Send message</button>
                          </form> --}}
                      </article><!--//contact-form-->
                      <aside class="page-sidebar  col-lg-3 offset-lg-1 col-md-4 offset-md-1">
                          <section class="widget has-divider">
                              <h3 class="title">Download Prospectus</h3>
                              <p>Donec pulvinar arcu lacus, vel aliquam libero scelerisque a. Cras mi tellus, vulputate eu eleifend at, consectetur fringilla lacus. Nulla ut purus.</p>
                              <a class="btn btn-theme" href="#"><i class="fas fa-download"></i>Download now</a>
                          </section><!--//widget-->

                          <section class="widget has-divider">
                              <h3 class="title">Postal Address</h3>
                              <p class="adr">
                                  <span class="adr-group">
                                      <span class="street-address">Rajiv Gandhi Institute of Medical Sciences</span><br>
                                      <span class="region">Puttampalli</span><br>
                                      <span class="region">Kadapa(District)</span><br>
                                      <span class="postal-code">Andhra Pradesh - 516002</span><br>
                                      {{-- <span class="country-name">516002</span> --}}
                                  </span>
                              </p>
                          </section><!--//widget-->

                          <section class="widget">
                              <h3 class="title">All Enquiries</h3>
                              <p class="tel"><i class="fas fa-phone"></i>Tel: 08562 200038</p>
                              <p class="email"><i class="fas fa-envelope"></i>Email: <a href="#">directorrimskadapa@yahoo.com</a></p>
                          </section>
                      </aside><!--//page-sidebar-->
                  </div>
                  {{-- <div class="page-row">
                      <article class="map-section">
                          <h3 class="title">How to find us</h3>
                          <div class="gmap-wrapper" id="map">
                              <!--//You need to embed your own google map below-->
                              <!--//Ref: https://support.google.com/maps/answer/144361?co=GENIE.Platform%3DDesktop&hl=en -->
                              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2485.985798395094!2d-2.6051732483185885!3d51.458417179527075!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48718ddbdfd292fb%3A0x2f0b60f89b4b6d56!2sUniversity+of+Bristol!5e0!3m2!1sen!2suk!4v1469704137699" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                          </div><!--//gmap-wrapper-->
                      </article><!--//map-->
                  </div><!--//page-row--> --}}
                </div><!--//page-content-->
            </div><!--//page-->
        </div>
      </div>

    <!-- ******FOOTER****** -->
    @include('frontend.footer')
    <!--//footer-->

    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/popper.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/back-to-top.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/flexslider/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/jflickrfeed/jflickrfeed.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/js/main.js"></script>


    <script type="text/javascript" src="{{ config('app.url') }}/assets/js/demo/theme-switcher.js"></script>

</body>

</html>
