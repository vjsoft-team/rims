@php
  use App\About;
@endphp
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themes.3rdwavemedia.com/college-green/bs4/3.0/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Oct 2018 10:44:10 GMT -->
<head>
    <title>About Us | RIMS Kadapa</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    <!-- FontAwesome JS-->
    <script defer src="{{ config('app.url') }}/use.fontawesome.com/releases/v5.1.0/js/all.js" integrity="sha384-3LK/3kTpDE/Pkp8gTNp2gR/2gOiwQ6QaO7Td0zV76UFJVhqLl4Vl3KL1We6q6wR9" crossorigin="anonymous"></script>
    <!-- Global CSS -->
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/plugins/bootstrap/css/bootstrap.min.css">
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/plugins/flexslider/flexslider.css">
    <!-- Theme CSS -->
    <link id="theme-style" rel="stylesheet" href="{{ config('app.url') }}/assets/css/theme-1.css">

</head>

<body>
    <div class="wrapper">
        <!-- ******HEADER****** -->
        @include('frontend.header')
        <!--//header-->

        <!-- ******NAV****** -->
        <!--//main-nav-container-->
        @php
          $page_id = 'about';
        @endphp

        @php
          $new = About::all();
        @endphp

        @include('frontend.navbar')
        <!-- ******CONTENT****** -->

        <div class="content container">
            <div class="page-wrapper">
                <header class="page-heading clearfix">
                    <h1 class="heading-title float-left">About</h1>
                    <div class="breadcrumbs float-right">
                        <ul class="breadcrumbs-list">
                            <li class="breadcrumbs-label">You are here:</li>
                            <li><a href="/">Home</a><i class="fas fa-angle-right"></i></li>
                            {{-- <li class="current">About</li> --}}
                            <li>
                            <?php $link = "" ?>
            @for($i = 1; $i <= count(Request::segments()); $i++)
             @if($i < count(Request::segments()) & $i > 0)
           <?php $link .= "/" . Request::segment($i); ?>
          <a href="<?= $link ?>">{{ ucwords(str_replace('-',' ',Request::segment($i)))}}</a>
             @else {{ucwords(str_replace('-',' ',Request::segment($i)))}}
         @endif
         @endfor
         </li>
                        </ul>
                    </div><!--//breadcrumbs-->
                </header>
                <div class="page-content">
                    <div class="row page-row">
                        <article class="welcome col-lg-8 col-md-7 col-12">
                            <h3 class="title">Welcome to RIMS</h3>
                            <p><img class="img-fluid" style="width:750px;height:300px" src="assets/images/about.jpg" alt="" /></p>
                            @foreach ($new as $key)
                              <p>{!!$key->content!!}</p>
                            @endforeach
                        </article><!--//page-content-->
                        <aside class="page-sidebar  col-lg-3 offset-lg-1 col-md-4 offset-md-1">
                            <section class="widget has-divider">
                                <h3 class="title">Job Vacancies</h3>
                                <ul class="job-list custom-list-style">
                                    <li><i class="fas fa-caret-right"></i><a href="#">Learning Support Assistant</a></li>
                                    <li><i class="fas fa-caret-right"></i><a href="#">Lecturer - Business Studies</a></li>
                                    <li><i class="fas fa-caret-right"></i><a href="#">Lecturer - Computer Science</a></li>
                                    <li><i class="fas fa-caret-right"></i><a href="#">Administrative Assistant</a></li>
                                </ul>
                                <a class="btn btn-theme" href="jobs.html">Find out more</a>
                            </section><!--//widget-->
                            <section class="widget has-divider">
                                <h3 class="title">Video tour</h3>
                                <div class="embed-responsive embed-responsive-16by9">
	                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/Ll7LKTBeJUc" frameborder="0" allowfullscreen=""></iframe>
                                </div>
                            </section><!--//widget-->
                            <section class="widget">
                                <h3 class="title">Contact</h3>
                                <p>Suspendisse mollis neque augue, vitae malesuada mi dictum quis. Ut mollis purus vel orci aliquam sagittis. Aliquam erat volutpat. </p>
                                <p class="tel"><i class="fas fa-phone"></i>Tel: 0800 123 4567</p>
                                <p class="email"><i class="fas fa-envelope"></i>Email: <a href="#">enquires@website.com</a></p>
                            </section><!--//widget-->
                        </aside>
                    </div><!--//page-row-->
                </div><!--//page-content-->
            </div><!--//page-->
        </div>
      </div>

    <!-- ******FOOTER****** -->
    @include('frontend.footer')
    <!--//footer-->

    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/popper.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/back-to-top.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/flexslider/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/jflickrfeed/jflickrfeed.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/js/main.js"></script>

    <!-- Theme Switcher (REMOVE ON YOUR PRODUCTION SITE) -->
    <script type="text/javascript" src="{{ config('app.url') }}/assets/js/demo/theme-switcher.js"></script>

</body>

<!-- Mirrored from themes.3rdwavemedia.com/college-green/bs4/3.0/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Oct 2018 10:44:29 GMT -->
</html>
