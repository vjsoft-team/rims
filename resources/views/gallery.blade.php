@php
  use App\GalleryEvent;
@endphp
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themes.3rdwavemedia.com/college-green/bs4/3.0/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Oct 2018 10:44:10 GMT -->
<head>
    <title>Gallery | RIMS Kadapa</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    <!-- FontAwesome JS-->
    <script defer src="{{ config('app.url') }}/use.fontawesome.com/releases/v5.1.0/js/all.js" integrity="sha384-3LK/3kTpDE/Pkp8gTNp2gR/2gOiwQ6QaO7Td0zV76UFJVhqLl4Vl3KL1We6q6wR9" crossorigin="anonymous"></script>
    <!-- Global CSS -->
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/plugins/bootstrap/css/bootstrap.min.css">
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/plugins/flexslider/flexslider.css">
    <!-- Theme CSS -->
    <link id="theme-style" rel="stylesheet" href="{{ config('app.url') }}/assets/css/theme-1.css">

</head>

<body>
    <div class="wrapper">
        <!-- ******HEADER****** -->
        @include('frontend.header')
        <!--//header-->

        <!-- ******NAV****** -->
        <!--//main-nav-container-->
        @php
          $page_id = 'gallery';
        @endphp

        @php
          $new = GalleryEvent::all();
        @endphp

        @include('frontend.navbar')
        <!-- ******CONTENT****** -->

        <div class="content container">
            <div class="page-wrapper">
                <header class="page-heading clearfix">
                    <h1 class="heading-title float-left">Gallery</h1>
                    <div class="breadcrumbs float-right">
                        <ul class="breadcrumbs-list">
                            <li class="breadcrumbs-label">You are here:</li>
                            <li><a href="/">Home</a><i class="fas fa-angle-right"></i></li>
                            {{-- <li class="current">About</li> --}}
                            <li>
                            <?php $link = "" ?>
            @for($i = 1; $i <= count(Request::segments()); $i++)
             @if($i < count(Request::segments()) & $i > 0)
           <?php $link .= "/" . Request::segment($i); ?>
          <a href="<?= $link ?>">{{ ucwords(str_replace('-',' ',Request::segment($i)))}}</a>
             @else {{ucwords(str_replace('-',' ',Request::segment($i)))}}
         @endif
         @endfor
         </li>
                        </ul>
                    </div><!--//breadcrumbs-->
                </header>
                <div class="page-content">
                    <div class="page-row">
                        <p>Click the Below Events to view All Images</p>
                        {{-- <p class="page-row text-muted">Image credit: <a href="http://www.flickr.com/photos/francisco_osorio/" target="_blank">Francisco Osorio</a>, <a href="http://creativecommons.org/licenses/by/2.0/deed.en" target="_blank">Creative Commons 2.0 license</a></p> --}}
                    </div>
                    <div class="row page-row">
                      @foreach ($new as $key)
                        <div class="col-md-3 col-12 text-center">
                            <div class="album-cover" style="min-height:auto;">
                              @php

                              @endphp
                                <a href="{{ config('app.url') }}/gallery/{{ $key->id}}"><img class="img-fluid" src="{{ config('app.url') }}/store/{{$key->image}}" alt="" /></a>
                                <div class="desc">
                                  <h4><small><a href="{{ config('app.url') }}/gallery/{{ $key->id}}">{{$key->title}}</a></small></h4>
                                  {{-- <p>Integer ornare nisl tortor, sed condimentum metus pulvinar ut. Etiam ac pretium nunc. Donec porttitor erat non nibh pellentesque vehicula. Vestibulum tincidunt</p> --}}
                                </div>
                            </div>
                        </div>
                      @endforeach
                    </div><!--//page-row-->

                </div><!--//page-content-->
            </div><!--//page-->
        </div>
      </div>

    <!-- ******FOOTER****** -->
    @include('frontend.footer')
    <!--//footer-->

    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/popper.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/back-to-top.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/flexslider/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/jflickrfeed/jflickrfeed.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/js/main.js"></script>

    <!-- Theme Switcher (REMOVE ON YOUR PRODUCTION SITE) -->
    <script type="text/javascript" src="{{ config('app.url') }}/assets/js/demo/theme-switcher.js"></script>

</body>

<!-- Mirrored from themes.3rdwavemedia.com/college-green/bs4/3.0/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Oct 2018 10:44:29 GMT -->
</html>
