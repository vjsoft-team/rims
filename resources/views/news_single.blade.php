@php
  use App\Newese;
@endphp
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themes.3rdwavemedia.com/college-green/bs4/3.0/news-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Oct 2018 10:44:32 GMT -->
<head>
    <title>News Single | RIMS Kadapa</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    <!-- FontAwesome JS-->
    <script defer src="{{ config('app.url') }}/use.fontawesome.com/releases/v5.1.0/js/all.js" integrity="sha384-3LK/3kTpDE/Pkp8gTNp2gR/2gOiwQ6QaO7Td0zV76UFJVhqLl4Vl3KL1We6q6wR9" crossorigin="anonymous"></script>
    <!-- Global CSS -->
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/plugins/bootstrap/css/bootstrap.min.css">
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/plugins/flexslider/flexslider.css">
    <!-- Theme CSS -->
    <link id="theme-style" rel="stylesheet" href="{{ config('app.url') }}/assets/css/theme-1.css">

</head>

<body>
    <div class="wrapper">
        <!-- ******HEADER****** -->
          @include('frontend.header')
        <!--//header-->

        @php
          $page_id = 'news';
        @endphp

        <!-- ******NAV****** -->
        @include('frontend.navbar')
        <!--//main-nav-container-->

        <!-- ******CONTENT****** -->
        <div class="content container">
            <div class="page-wrapper">
                <header class="page-heading clearfix">
                    <h1 class="heading-title float-left">{{$data->heading}}</h1>
                    <div class="breadcrumbs float-right">
                        <ul class="breadcrumbs-list">
                            <li class="breadcrumbs-label">You are here:</li>
                            <li><a href="/">Home</a><i class="fas fa-angle-right"></i></li>
                            <li><a href="/news">News</a><i class=""></i></li>
                        </ul>
                    </div><!--//breadcrumbs-->
                </header>
                <div class="page-content">
                    <div class="row page-row">
                        <div class="news-wrapper col-lg-8 col-md-7 col-12">
                            <article class="news-item">
                                <p class="meta text-muted">By: <a href="#">Admin</a> | Posted on: 26 Jan 2014</p>
                                <p class="featured-image"><img class="img-fluid" src="{{ config('app.url') }}/store/{{$data->image}}" alt="no" /></p>
                                <p>{!!$data->content!!}</p>
                            </article><!--//news-item-->
                        </div><!--//news-wrapper-->
                        @php
                          $data = Newese::all();
                        @endphp
                        <aside class="page-sidebar  col-lg-3 offset-lg-1 col-md-4 offset-md-1 col-12">
                            <section class="widget has-divider">
                                <h3 class="title">Other News</h3>
                                @foreach ($data as $key)
                                <article class="news-item row">
                                    <figure class="thumb col-xl-2 col-3">
                                        <img src="{{ config('app.url') }}/store/{{$key->image}}" alt="">
                                    </figure>
                                    <div class="details col-xl-10 col-9">
                                        <h4 class="title"><a href="{{config('app.url')}}/news-view/{{$key->id}}">{{$key->heading}}</a></h4>
                                    </div>
                                </article><!--//news-item-->
                              @endforeach
                                <!--//news-item-->
                                <!--//news-item-->
                                <!--//news-item-->
                            </section><!--//widget-->
                            {{-- <section class="widget has-divider">
                                <h3 class="title">Upcoming Events</h3>
                                <article class="events-item row page-row">
                                        <div class="date-label-wrapper col-lg-3 col-4">
                                            <p class="date-label">
                                                <span class="month">FEB</span>
                                                <span class="date-number">18</span>
                                            </p>
                                        </div><!--//date-label-wrapper-->
                                        <div class="details col-lg-9 col-8">
                                            <h5 class="title">Open Day</h5>
                                            <p class="time text-muted">10:00am - 18:00pm<br>East Campus</p>
                                        </div><!--//details-->
                                </article>
                                <article class="events-item row page-row">
                                    <div class="date-label-wrapper col-lg-3 col-4">
                                        <p class="date-label">
                                            <span class="month">SEP</span>
                                            <span class="date-number">06</span>
                                        </p>
                                    </div><!--//date-label-wrapper-->
                                    <div class="details col-lg-9 col-8">
                                        <h5 class="title">E-learning at College Green</h5>
                                        <p class="time text-muted">10:00am - 16:00pm<br>Learning Center</p>
                                    </div><!--//details-->
                                </article>
                                <article class="events-item row page-row">
                                    <div class="date-label-wrapper col-lg-3 col-4">
                                        <p class="date-label">
                                            <span class="month">JUN</span>
                                            <span class="date-number">23</span>
                                        </p>
                                    </div><!--//date-label-wrapper-->
                                    <div class="details col-lg-9 col-8">
                                        <h5 class="title">Career Fair</h5>
                                        <p class="time text-muted">09:45am - 16:00pm<br>Library</p>
                                    </div><!--//details-->
                                </article>
                            </section><!--//widget--> --}}
                        </aside>
                    </div><!--//page-row-->
                </div><!--//page-content-->
            </div><!--//page-->
        </div><!--//content-->
    </div><!--//wrapper-->

    <!-- ******FOOTER****** -->
      @include('frontend.footer')
    <!--//footer-->

    <!-- *****CONFIGURE STYLE****** -->
    <!--//config-wrapper-->

    <!-- Javascript -->
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/popper.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/back-to-top.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/flexslider/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/plugins/jflickrfeed/jflickrfeed.min.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/assets/js/main.js"></script>

    <!-- Theme Switcher (REMOVE ON YOUR PRODUCTION SITE) -->
    <script type="text/javascript" src="{{ config('app.url') }}/assets/js/demo/theme-switcher.js"></script>

</body>

<!-- Mirrored from themes.3rdwavemedia.com/college-green/bs4/3.0/news-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Oct 2018 10:44:32 GMT -->
</html>
