<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/', 'PagesController@home');
Route::get('/about', 'PagesController@about');
Route::get('/gallery', 'PagesController@gallery');
Route::get('/gallery/{id}', 'PagesController@editp');
Route::get('/news', 'PagesController@news');
Route::get('/contact', 'PagesController@contact');
Route::get('/{name}', 'PagesController@index');
//News- Single//
Route::get('news-view/{id}', 'PagesController@newsedit');
