-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 21, 2018 at 01:09 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rims`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `content`, `created_at`, `updated_at`) VALUES
(1, '<p>The Rajiv Gandhi Institute of Medical Sciences, Kadapa was established in 2006 with 188 acres land area comprising of RIMS Medical College, General Hospital(OP and IP) buildings, B.Sc Nursing College, Dental College, Hostels to MBBS Students, House Surgeons &amp;&nbsp; Staff Quarters etc.,&nbsp;</p>\r\n<p>The RIMS Medical College is functioning from 2006 with intake capacity of 150 MBBS Seats.&nbsp; Now it is completed more than 12 years and successfully completed 6 batches of MBBS Students. Para Medical Courses are also started from 2009 functioning.PG Courses started from 2013-14 academic year in 7 Broad specialties and 2 pre clinical Departments</p>\r\n<ul>\r\n<li>1500 &ndash; 2000 Out Patients, 750 Teaching Beds.</li>\r\n<li style=\"text-align: left;\">Fully equipped Maternity &amp; Gynecology Department.</li>\r\n<li style=\"text-align: left;\">General Surgery.</li>\r\n<li style=\"text-align: left;\">Burns ward for Burns cases.</li>\r\n<li style=\"text-align: left;\">Orthopedics, for Poly trauma care with C-Arm facility, Arthroscopy &amp; TKR.</li>\r\n<li style=\"text-align: left;\">General Medicine.</li>\r\n<li style=\"text-align: left;\">ICU, ICCU (Coronary Cardiac Unit) with ventilator facility</li>\r\n<li>Haemodialysis (BBran Medical care).</li>\r\n<li>Fully equipped Blood Bank with component facility.</li>\r\n<li>Pediatrics &amp; Neonatology with NICU, PICU &amp; Neonatal Ventilator facility.</li>\r\n<li>ENT.</li>\r\n<li>Ophthalmic.</li>\r\n<li>TB&amp;CD.</li>\r\n<li style=\"text-align: left;\">Psychiatry.</li>\r\n<li style=\"text-align: left;\">&nbsp;Advanced 24 hour Diagnostic Central Laboratories with fully Automated Analyser, Chemiluminisence, ABG, 5part Cell Counter, ELISA for Dengue, SWINE flu&nbsp; &nbsp;Mini PCR etc.</li>\r\n<li style=\"text-align: left;\">Digital X&ndash;Ray machines, U/c scanning &amp; 15 slice CT Scan.</li>\r\n</ul>\r\n<p>RIMS Kadapa has achieved and evolved into the present status.</p>\r\n<ul>\r\n<li>Central Oxygen System restored to functioning.</li>\r\n<li>Platelet Counting machine established</li>\r\n<li>Dr. NTR Vaidya Seva is functioning and 5th position in the State target.</li>\r\n<li>Purified drinking water facility (RO Plant)&nbsp; in Genral Hospital and students&nbsp; Hostels.</li>\r\n<li>In 2007 Dr. Jaswant Reddy, RIMS Medical College Student has won 3 Gold Medals in Sports at Ranga Raya Medical College, Kakinada</li>\r\n<li>MCI recognized MD DVL in course 2017 (2 seats).</li>\r\n<li>PG Seats of broad specialties are increased from 16 to 32 in 2017 -18 AY.</li>\r\n<li>MCI recognized MS Ophthalmology course in 2018 (2 seats).</li>\r\n<li>Further UG seats from 150 to 200 are going to be enhanced in the coming academic years under Central Govt Scheme.</li>\r\n<li>NABH Accreditations is under progress.</li>\r\n</ul>\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>', '2018-11-15 01:38:12', '2018-11-15 01:38:12');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `slider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `heading` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_heading` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `slider`, `heading`, `sub_heading`, `created_at`, `updated_at`) VALUES
(1, 'banners\\October2018\\c3Wjp4kjckuV7oDKbLdI.jpg', 'RIMS Kadapa', 'Established in the Year of 2006', '2018-10-31 00:42:24', '2018-10-31 00:42:24'),
(2, 'banners\\October2018\\cFEpqCHdlXHxa1IMnSOA.jpg', 'RIMS Kadapa', 'Improving Quality health Care', '2018-10-31 00:43:23', '2018-10-31 00:43:23'),
(3, 'banners\\October2018\\HCcQPsWiIeUNSRcKJj8M.jpg', 'RIMS', 'Test', '2018-10-31 00:44:01', '2018-10-31 00:44:01');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'locale', 'text', 'Locale', 0, 1, 1, 1, 1, 0, NULL, 12),
(12, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(13, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(14, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(15, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(16, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(17, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(18, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(19, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(20, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(21, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(22, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(23, 5, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(24, 5, 'slider', 'image', 'Slider', 0, 1, 1, 1, 1, 1, '{}', 2),
(25, 5, 'heading', 'text', 'Heading', 0, 1, 1, 1, 1, 1, '{}', 3),
(26, 5, 'sub_heading', 'text_area', 'Sub Heading', 0, 1, 1, 1, 1, 1, '{}', 4),
(27, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(28, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(29, 6, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(30, 6, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(31, 6, 'body', 'rich_text_box', 'Body', 0, 1, 1, 1, 1, 1, '{}', 3),
(32, 6, 'course_structure', 'text_area', 'Course Structure', 0, 1, 1, 1, 1, 1, '{}', 4),
(33, 6, 'fees', 'text_area', 'Fees', 0, 1, 1, 1, 1, 1, '{}', 5),
(34, 6, 'faculty', 'text_area', 'Faculty', 0, 1, 1, 1, 1, 1, '{}', 6),
(35, 6, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 7),
(36, 6, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(37, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(38, 6, 'slug', 'text', 'Slug', 0, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"}}', 10),
(39, 7, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(40, 7, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 2),
(41, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
(42, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(43, 8, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(44, 8, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(45, 8, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 3),
(46, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(47, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(48, 9, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(49, 9, 'event_title', 'text', 'Event Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(50, 9, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 3),
(51, 9, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(52, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(53, 9, 'gallery_image_belongsto_gallery_event_relationship', 'relationship', 'gallery_events', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\GalleryEvent\",\"table\":\"gallery_events\",\"type\":\"belongsTo\",\"column\":\"event_title\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"abouts\",\"pivot\":\"0\",\"taggable\":\"0\"}', 6),
(54, 10, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(55, 10, 'heading', 'text', 'Heading', 0, 1, 1, 1, 1, 1, '{}', 2),
(56, 10, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 3),
(57, 10, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 4),
(58, 10, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(59, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', '', '', 1, 0, NULL, '2018-10-30 06:44:49', '2018-10-30 06:44:49'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2018-10-30 06:44:50', '2018-10-30 06:44:50'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2018-10-30 06:44:50', '2018-10-30 06:44:50'),
(4, 'banner', 'banner', 'Banner', 'Banners', 'voyager-wallet', 'App\\Banner', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-10-31 00:36:28', '2018-10-31 00:36:28'),
(5, 'banners', 'banners', 'Banner', 'Banners', NULL, 'App\\Banner', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-10-31 00:39:17', '2018-10-31 00:39:17'),
(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'App\\Page', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-11-01 01:04:35', '2018-11-01 01:04:35'),
(7, 'abouts', 'abouts', 'About', 'Abouts', 'voyager-group', 'App\\About', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-11-15 01:03:05', '2018-11-15 01:03:05'),
(8, 'gallery_events', 'gallery-events', 'Gallery Event', 'Gallery Events', 'voyager-credit-cards', 'App\\GalleryEvent', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-11-15 04:33:31', '2018-11-15 04:33:31'),
(9, 'gallery_images', 'gallery-images', 'Gallery Image', 'Gallery Images', NULL, 'App\\GalleryImage', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-11-15 04:48:51', '2018-11-15 04:52:10'),
(10, 'neweses', 'neweses', 'News', 'News', 'voyager-news', 'App\\Newese', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-11-21 01:08:08', '2018-11-21 01:08:08');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_events`
--

CREATE TABLE `gallery_events` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gallery_events`
--

INSERT INTO `gallery_events` (`id`, `title`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Health Fest 218', 'gallery-events\\November2018\\vjneMMpCXayY0eFHkQTa.jpg', '2018-11-15 04:40:10', '2018-11-15 04:40:10'),
(2, 'Vanam Manam 14 July 218', 'gallery-events\\November2018\\sj7l7LtBX0C5zkbVWY25.jpeg', '2018-11-15 04:44:02', '2018-11-15 04:44:02'),
(3, 'HDS Meeting', 'gallery-events\\November2018\\RtjDBZfFXYd57QL8TRye.jpeg', '2018-11-15 07:14:59', '2018-11-15 07:14:59');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_images`
--

CREATE TABLE `gallery_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `event_title` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gallery_images`
--

INSERT INTO `gallery_images` (`id`, `event_title`, `image`, `created_at`, `updated_at`) VALUES
(1, '1', 'gallery-images\\November2018\\VATmCyLwdMkIBL4xpr6j.jpg', '2018-11-15 04:52:41', '2018-11-15 04:52:41'),
(2, '1', 'gallery-images\\November2018\\60NKMDrGX9MJcvB1rStj.jpg', '2018-11-15 04:52:58', '2018-11-15 04:52:58'),
(3, '1', 'gallery-images\\November2018\\pSFqxyBqSaIbhP7f5UOr.jpeg', '2018-11-15 04:53:09', '2018-11-15 04:53:09'),
(4, '2', 'gallery-images\\November2018\\FowPbFqqp8VO3kl0tRHK.jpeg', '2018-11-15 04:53:38', '2018-11-15 04:53:38'),
(5, '2', 'gallery-images\\November2018\\vE63AVy4PAG7mmmCcnhp.jpeg', '2018-11-15 04:53:52', '2018-11-15 04:53:52');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2018-10-30 06:44:50', '2018-10-30 06:44:50'),
(2, 'frontend', '2018-11-01 00:47:15', '2018-11-01 00:47:15');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2018-10-30 06:44:50', '2018-10-30 06:44:50', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 9, '2018-10-30 06:44:50', '2018-11-15 05:06:11', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 8, '2018-10-30 06:44:50', '2018-11-15 05:06:11', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 7, '2018-10-30 06:44:50', '2018-11-15 05:06:11', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 10, '2018-10-30 06:44:50', '2018-11-15 05:06:11', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2018-10-30 06:44:50', '2018-11-02 00:48:41', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2018-10-30 06:44:50', '2018-11-15 05:06:11', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2018-10-30 06:44:50', '2018-11-15 05:06:11', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2018-10-30 06:44:50', '2018-11-15 05:06:11', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 11, '2018-10-30 06:44:50', '2018-11-15 05:06:11', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2018-10-30 06:44:53', '2018-11-15 05:06:11', 'voyager.hooks', NULL),
(13, 1, 'Banners', '', '_self', 'voyager-people', '#000000', NULL, 2, '2018-10-31 00:39:17', '2018-11-02 00:48:48', 'voyager.banners.index', 'null'),
(15, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, NULL, 6, '2018-11-01 01:04:35', '2018-11-15 05:06:11', 'voyager.pages.index', NULL),
(16, 2, 'Home', '/', '_self', NULL, '#000000', NULL, 1, '2018-11-01 01:08:32', '2018-11-02 02:59:21', NULL, ''),
(17, 2, 'About us', 'about', '_self', NULL, '#000000', NULL, 2, '2018-11-01 01:09:21', '2018-11-15 00:08:58', NULL, ''),
(18, 2, 'Departments', '', '_self', NULL, '#000000', NULL, 3, '2018-11-01 01:10:27', '2018-11-15 00:02:36', NULL, ''),
(19, 2, 'Anotomy', 'anotomy', '_self', NULL, '#000000', 18, 1, '2018-11-01 01:11:19', '2018-11-14 23:55:42', NULL, ''),
(20, 2, 'zoology', 'zoology', '_self', NULL, '#000000', 18, 2, '2018-11-01 07:51:45', '2018-11-02 01:53:44', NULL, ''),
(22, 2, 'Contact', 'contact', '_self', NULL, '#000000', NULL, 9, '2018-11-02 02:58:55', '2018-11-15 00:38:45', NULL, ''),
(23, 2, 'News', 'news', '_self', NULL, '#000000', NULL, 5, '2018-11-15 00:36:59', '2018-11-21 03:00:07', NULL, ''),
(24, 2, 'Tenders', '', '_self', NULL, '#000000', NULL, 6, '2018-11-15 00:37:15', '2018-11-15 00:38:45', NULL, ''),
(25, 2, 'UG Academic', '#', '_self', NULL, '#000000', NULL, 7, '2018-11-15 00:37:52', '2018-11-15 00:38:45', NULL, ''),
(26, 2, 'PG Academic', '#', '_self', NULL, '#000000', NULL, 8, '2018-11-15 00:38:24', '2018-11-15 00:38:45', NULL, ''),
(27, 2, 'Gallery', 'gallery', '_self', NULL, '#000000', NULL, 4, '2018-11-15 00:46:37', '2018-11-15 05:07:12', NULL, ''),
(28, 1, 'Abouts', '', '_self', 'voyager-group', NULL, NULL, 3, '2018-11-15 01:03:05', '2018-11-15 05:06:14', 'voyager.abouts.index', NULL),
(29, 1, 'Gallery Events', '', '_self', 'voyager-credit-cards', NULL, NULL, 4, '2018-11-15 04:33:31', '2018-11-15 05:06:14', 'voyager.gallery-events.index', NULL),
(30, 1, 'Gallery Images', '', '_self', 'voyager-photos', '#000000', NULL, 5, '2018-11-15 04:48:51', '2018-11-21 01:03:24', 'voyager.gallery-images.index', 'null'),
(31, 1, 'News', '', '_self', 'voyager-news', NULL, NULL, 12, '2018-11-21 01:08:08', '2018-11-21 01:08:08', 'voyager.neweses.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1);

-- --------------------------------------------------------

--
-- Table structure for table `neweses`
--

CREATE TABLE `neweses` (
  `id` int(10) UNSIGNED NOT NULL,
  `heading` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `neweses`
--

INSERT INTO `neweses` (`id`, `heading`, `content`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Health Fest 2018', '<p><span style=\"color: #000000; font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose&nbsp;</span></p>', 'neweses\\November2018\\OIik6Fdr1WvOALpt9667.jpg', '2018-11-21 01:12:00', '2018-11-21 01:44:25'),
(2, 'Vanam Manam 2018', '<p><span style=\"color: #000000; font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\">Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>', 'neweses\\November2018\\FnxnqinATF48k9vEPFGG.jpeg', '2018-11-21 01:14:22', '2018-11-21 01:14:22'),
(3, 'Independence Day', '<p><span style=\"color: #000000; font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200&nbsp;</span></p>', 'neweses\\November2018\\TJwfFuFF8i8VtpzGn2fT.jpg', '2018-11-21 01:19:00', '2018-11-21 04:39:15'),
(4, 'Special Chief Secretary Vist  2018', '<p><span style=\"color: #000000; font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the&nbsp;</span></p>', 'neweses\\November2018\\sbqdeCEGo5rFDpuI2V0n.jpeg', '2018-11-21 01:21:13', '2018-11-21 01:21:13'),
(5, 'HDS Meeting', '<p><span style=\"color: #000000; font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\",&nbsp;</span></p>', 'neweses\\November2018\\z8RrY0cp2d03aJ3tfU97.JPG', '2018-11-21 01:28:00', '2018-11-21 04:29:54'),
(6, 'Official Meeting', '<p><span style=\"color: #000000; font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200&nbsp;</span></p>', 'neweses\\November2018\\jqcLe7Y9sFxKIN4nGdxJ.JPG', '2018-11-21 01:30:00', '2018-11-21 04:39:52');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci,
  `course_structure` longtext COLLATE utf8mb4_unicode_ci,
  `fees` longtext COLLATE utf8mb4_unicode_ci,
  `faculty` longtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `body`, `course_structure`, `fees`, `faculty`, `image`, `created_at`, `updated_at`, `slug`) VALUES
(1, 'Anotomy', '<p><span style=\"color: #000000; font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident,</span></p>', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words', 'There are many variations of passages of Lorem Ipsum availabl -85000', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words', 'pages\\November2018\\1dCW3hoCiaGBiIpBCdSb.jpg', '2018-11-01 01:13:00', '2018-11-01 04:45:59', 'anotomy'),
(2, 'Zoology', '<p>Test</p>', 'Test', 'Test', 'Test', NULL, '2018-11-01 07:52:55', '2018-11-01 07:52:55', 'zoology');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2018-10-30 06:44:51', '2018-10-30 06:44:51'),
(2, 'browse_bread', NULL, '2018-10-30 06:44:51', '2018-10-30 06:44:51'),
(3, 'browse_database', NULL, '2018-10-30 06:44:51', '2018-10-30 06:44:51'),
(4, 'browse_media', NULL, '2018-10-30 06:44:51', '2018-10-30 06:44:51'),
(5, 'browse_compass', NULL, '2018-10-30 06:44:51', '2018-10-30 06:44:51'),
(6, 'browse_menus', 'menus', '2018-10-30 06:44:51', '2018-10-30 06:44:51'),
(7, 'read_menus', 'menus', '2018-10-30 06:44:51', '2018-10-30 06:44:51'),
(8, 'edit_menus', 'menus', '2018-10-30 06:44:51', '2018-10-30 06:44:51'),
(9, 'add_menus', 'menus', '2018-10-30 06:44:51', '2018-10-30 06:44:51'),
(10, 'delete_menus', 'menus', '2018-10-30 06:44:51', '2018-10-30 06:44:51'),
(11, 'browse_roles', 'roles', '2018-10-30 06:44:51', '2018-10-30 06:44:51'),
(12, 'read_roles', 'roles', '2018-10-30 06:44:51', '2018-10-30 06:44:51'),
(13, 'edit_roles', 'roles', '2018-10-30 06:44:51', '2018-10-30 06:44:51'),
(14, 'add_roles', 'roles', '2018-10-30 06:44:51', '2018-10-30 06:44:51'),
(15, 'delete_roles', 'roles', '2018-10-30 06:44:51', '2018-10-30 06:44:51'),
(16, 'browse_users', 'users', '2018-10-30 06:44:51', '2018-10-30 06:44:51'),
(17, 'read_users', 'users', '2018-10-30 06:44:51', '2018-10-30 06:44:51'),
(18, 'edit_users', 'users', '2018-10-30 06:44:51', '2018-10-30 06:44:51'),
(19, 'add_users', 'users', '2018-10-30 06:44:51', '2018-10-30 06:44:51'),
(20, 'delete_users', 'users', '2018-10-30 06:44:51', '2018-10-30 06:44:51'),
(21, 'browse_settings', 'settings', '2018-10-30 06:44:51', '2018-10-30 06:44:51'),
(22, 'read_settings', 'settings', '2018-10-30 06:44:51', '2018-10-30 06:44:51'),
(23, 'edit_settings', 'settings', '2018-10-30 06:44:51', '2018-10-30 06:44:51'),
(24, 'add_settings', 'settings', '2018-10-30 06:44:51', '2018-10-30 06:44:51'),
(25, 'delete_settings', 'settings', '2018-10-30 06:44:51', '2018-10-30 06:44:51'),
(26, 'browse_hooks', NULL, '2018-10-30 06:44:53', '2018-10-30 06:44:53'),
(27, 'browse_banner', 'banner', '2018-10-31 00:36:28', '2018-10-31 00:36:28'),
(28, 'read_banner', 'banner', '2018-10-31 00:36:28', '2018-10-31 00:36:28'),
(29, 'edit_banner', 'banner', '2018-10-31 00:36:28', '2018-10-31 00:36:28'),
(30, 'add_banner', 'banner', '2018-10-31 00:36:28', '2018-10-31 00:36:28'),
(31, 'delete_banner', 'banner', '2018-10-31 00:36:28', '2018-10-31 00:36:28'),
(32, 'browse_banners', 'banners', '2018-10-31 00:39:17', '2018-10-31 00:39:17'),
(33, 'read_banners', 'banners', '2018-10-31 00:39:17', '2018-10-31 00:39:17'),
(34, 'edit_banners', 'banners', '2018-10-31 00:39:17', '2018-10-31 00:39:17'),
(35, 'add_banners', 'banners', '2018-10-31 00:39:17', '2018-10-31 00:39:17'),
(36, 'delete_banners', 'banners', '2018-10-31 00:39:17', '2018-10-31 00:39:17'),
(37, 'browse_pages', 'pages', '2018-11-01 01:04:35', '2018-11-01 01:04:35'),
(38, 'read_pages', 'pages', '2018-11-01 01:04:35', '2018-11-01 01:04:35'),
(39, 'edit_pages', 'pages', '2018-11-01 01:04:35', '2018-11-01 01:04:35'),
(40, 'add_pages', 'pages', '2018-11-01 01:04:35', '2018-11-01 01:04:35'),
(41, 'delete_pages', 'pages', '2018-11-01 01:04:35', '2018-11-01 01:04:35'),
(42, 'browse_abouts', 'abouts', '2018-11-15 01:03:05', '2018-11-15 01:03:05'),
(43, 'read_abouts', 'abouts', '2018-11-15 01:03:05', '2018-11-15 01:03:05'),
(44, 'edit_abouts', 'abouts', '2018-11-15 01:03:05', '2018-11-15 01:03:05'),
(45, 'add_abouts', 'abouts', '2018-11-15 01:03:05', '2018-11-15 01:03:05'),
(46, 'delete_abouts', 'abouts', '2018-11-15 01:03:05', '2018-11-15 01:03:05'),
(47, 'browse_gallery_events', 'gallery_events', '2018-11-15 04:33:31', '2018-11-15 04:33:31'),
(48, 'read_gallery_events', 'gallery_events', '2018-11-15 04:33:31', '2018-11-15 04:33:31'),
(49, 'edit_gallery_events', 'gallery_events', '2018-11-15 04:33:31', '2018-11-15 04:33:31'),
(50, 'add_gallery_events', 'gallery_events', '2018-11-15 04:33:31', '2018-11-15 04:33:31'),
(51, 'delete_gallery_events', 'gallery_events', '2018-11-15 04:33:31', '2018-11-15 04:33:31'),
(52, 'browse_gallery_images', 'gallery_images', '2018-11-15 04:48:51', '2018-11-15 04:48:51'),
(53, 'read_gallery_images', 'gallery_images', '2018-11-15 04:48:51', '2018-11-15 04:48:51'),
(54, 'edit_gallery_images', 'gallery_images', '2018-11-15 04:48:51', '2018-11-15 04:48:51'),
(55, 'add_gallery_images', 'gallery_images', '2018-11-15 04:48:51', '2018-11-15 04:48:51'),
(56, 'delete_gallery_images', 'gallery_images', '2018-11-15 04:48:51', '2018-11-15 04:48:51'),
(57, 'browse_neweses', 'neweses', '2018-11-21 01:08:08', '2018-11-21 01:08:08'),
(58, 'read_neweses', 'neweses', '2018-11-21 01:08:08', '2018-11-21 01:08:08'),
(59, 'edit_neweses', 'neweses', '2018-11-21 01:08:08', '2018-11-21 01:08:08'),
(60, 'add_neweses', 'neweses', '2018-11-21 01:08:08', '2018-11-21 01:08:08'),
(61, 'delete_neweses', 'neweses', '2018-11-21 01:08:08', '2018-11-21 01:08:08');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2018-10-30 06:44:51', '2018-10-30 06:44:51'),
(2, 'user', 'Normal User', '2018-10-30 06:44:51', '2018-10-30 06:44:51');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', '', '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', '', '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'rims@gmail.com', 'users\\October2018\\WCZIEP5yCfhpXIVdVRG0.png', NULL, '$2y$10$jTdjkor7B.x50zzwiSpwIeZZ7uGKUvoaA0olJNPDbQkthKFNOciuu', 'I3Xslndn2foL3UdMxiqHcc9FqicKOwPNSe8JPvxcI9ODtsDc3zE6AUMN8ChX', '{\"locale\":\"en\"}', '2018-10-30 06:59:10', '2018-10-30 07:48:14'),
(2, NULL, 'test', 'test@gmail.com', 'users\\October2018\\JkWke6YhhruQz6ijekF0.jpg', NULL, '$2y$10$13ILUXW4wru.cpaUUdeeF.SzECinvwC4S2lx07KyWkrSzb1HhQ4uy', NULL, '{\"locale\":\"en\"}', '2018-10-30 07:52:01', '2018-10-30 07:52:01');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `gallery_events`
--
ALTER TABLE `gallery_events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_images`
--
ALTER TABLE `gallery_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `neweses`
--
ALTER TABLE `neweses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `gallery_events`
--
ALTER TABLE `gallery_events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `gallery_images`
--
ALTER TABLE `gallery_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `neweses`
--
ALTER TABLE `neweses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
