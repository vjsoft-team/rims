<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Page;
use App\Newese;

class PagesController extends Controller
{
  public function index($name)
  {
    $newName = $name;
    // return $newName;
    // $page = new Page;
    $content = Page::where('slug', $newName)->get();
    return view('dynamic')->with('content', $content);
  }

  public function home()
  {
    return view('welcome');
  }
  public function about()
  {
    return view('about');
  }
  public function gallery()
  {
    return view('gallery');
  }
  public function editp($id)
    {
          $view = $id;
          return view('galleryView')->with('key', $view);

    }
  public function contact()
  {
    return view('contact');
  }

  public function newsedit($id)
    {
      $view = Newese::find($id);

      return view('news_single')->with('data', $view);
    }

    public function news()
    {
      return view('news');
    }
}
